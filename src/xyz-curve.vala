/* xyz-curve.vala
 *
 * Copyright (C) 2017 Muhannad Alrusayni <muhannad.alrusayni@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Xyz {
	/**
	 * This class represent a Curve in a 2D space and has four members {@link start}
	 * point, {@link end} point, {@link go_direction} vector and {@link arrive_direction}
	 * vector.
	 *
	 * this figure show you how this class looks like:<<BR>>
	 * {{xyz_curve_figure_1.svg}}
	 *
	 * @since 0.1
	 * @see Xyz.Line
	 */
	[Version (since = "0.1")]
	public class Curve : Object {
		/**
		 * Start point for this {@link Xyz.Curve}.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point start { get; set construct; }
		/**
		 * Go direction vector for this {@link Xyz.Curve}.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Vector go_direction { get; set construct; }
		/**
		 * End point for this {@link Xyz.Curve}.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Point end { get; set construct; }
		/**
		 * Arrive direction vector for this {@link Xyz.Curve}.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Vector arrive_direction { get; set construct; }

		/**
		 * move this {@link Xyz.Curve} to a new postion
		 *
		 * @param postion The new postion.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void move_to (Point postion) {
			var vector = new Vector.from_points (start, end);
			start.move_to (postion.x, postion.y);
			end.move_to (postion.x, postion.y);
			end.transfer (vector);
		}

		/**
		 * Transfer this {@link Xyz.Curve} by adding a vector to its postion.
		 *
		 * @param vector is the length and direction needed to transfer this curve.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void transfer (Vector vector) {
			start.transfer (vector);
			end.transfer (vector);
		}

		/**
		 * Create a new {@link Xyz.Curve} object from the given parameters.
		 *
		 * @param s_x Is the x for {@link start} point in this object.
		 * @param s_y Is the y for {@link start} point in this object.
		 * @param g_x Is the x for {@link go_direction} vector in this object.
		 * @param g_y Is the y for {@link go_direction} vector in this object.
		 * @param e_x Is the x for {@link end} point in this object.
		 * @param e_y Is the y for {@link end} point in this object.
		 * @param a_x Is the x for {@link arrive_direction} vector in this object.
		 * @param a_y Is the y for {@link arrive_direction} vector in this object.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Curve (double s_x = 0.0, double s_y = 0.0, double g_x = 0.0, double g_y = 0.0, double e_x = 0.0, double e_y = 0.0, double a_x = 0.0, double a_y = 0.0) {
			start = new Point (s_x, s_y);
			go_direction = new Vector (g_x, g_y);
			end = new Point (e_x, e_y);
			arrive_direction = new Vector (a_x, a_y);
		}

		/**
		 * Create a new {@link Xyz.Curve} object from the given parameters.
		 *
		 * @param start The {@link start} point.
		 * @param end The {@link end} point.
		 * @param go_direction The {@link go_direction} vector.
		 * @param arrive_direction The {@link arrive_direction} vector.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Curve.from_points (Point start, Point end, Vector go_direction = new Vector (), Vector arrive_direction = new Vector ()) {
			this (start.x, start.y, go_direction.x, go_direction.y, end.x, end.y, arrive_direction.x, arrive_direction.y);
		}

		/**
		 * Create a new {@link Xyz.Curve} object from the given line.
		 *
		 * Note: {@link go_direction} and {@link arrive_direction} will
		 * take ⟨0, 0⟩ value. So callling this method will create
		 * a cruve looks like a line.
		 *
		 * @param line The line that will be copied.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public Curve.from_line (Line line) {
			this.from_points (line.start, line.start, new Vector (0.0, 0.0), new Vector (0.0, 0.0));
		}

		/* TODO: add length() method */
		/* TODO: add centroid() method */
	}
}
