/* xyz-vector.vala
 *
 * Copyright (C) 2017 Muhannad Alrusayni <muhannad.alrusayni@gmail.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Math;

namespace Xyz {

	/**
	 * This class represent a vector in a 2D space and has two member {@link x}
	 * and {@link y}.
	 *
	 * Vector is a geometric object that has magnitude (or length) and direction.
	 * Vectors can be multiply, subtract, divide and added to other vectors and more.
	 *
	 * @since 0.1
	 * @see Point
	 */
	[Version (since = "0.1")]
	public class Vector : Object {
		/**
		 * x is the x-axis direction of this vector, some time it uses as the width
		 * of this vector.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double x { get; set construct; }
		/**
		 * y is the y-axis direction of this vector, some time it uses as the height
		 * of this vector.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double y { get; set construct; }

		/* TODO: add rotate method */

		/**
		 * Add other vector to this vector.
		 *
		 * @param other Other vector.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void add (Vector other) {
			x += other.x;
			y += other.y;
		}

		/**
		 * Subtract other vector from this vector.
		 *
		 * @param other Other vector.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void subtract (Vector other) {
			x -= other.x;
			y -= other.y;
		}

		/**
		 * Multiply other vector with this vector.
		 *
		 * @param other Other vector.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void multiply (Vector other) {
			x *= other.x;
			y *= other.y;
		}

		/**
		 * Divide this vector on other vector.
		 *
		 * @param other Other vector.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void divide (Vector other) {
			x /= other.x;
			y /= other.y;
		}

		/**
		 * Scale this vector by the gevin factor
		 *
		 * @param factor number of times this vector will be scaled
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void scale (double factor) {
			x *= factor;
			y *= factor;
		}

		/**
		 * Return the length of this vector.
		 *
		 * @return the length.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double length () {
			return (sqrt (pow(x, 2.0) + pow(y, 2.0)));
		}

		/**
		 * Normalize this vector.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void normalize () {
			var len = length ();
			x /= len;
			y /= len;
		}

		/**
		 * Negates the point,
		 *
		 * e.g. point A(5, 10) will be A(-5, -10) after
		 * calling this method.
		 *
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void negates () {
			x = -x;
			y = -y;
		}

		/**
		 * Return the dot product of this vector and other vector.
		 *
		 * @param other Other vector.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public double dot_product (Vector other) {
			return (x * other.x) + (y * other.y);
		}

		/**
		 * Check if this vector and other vector have the same length and
		 * direction and.
		 *
		 * @param other Other Vector.
		 * @return true if they have the same length and direction.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public bool equal (Vector other) {
			return (x == other.x && y == other.y);
		}

		/**
		 * Check if this vector is opposite with other vector and have the
		 * same length.
		 *
		 * @param other Other vector.
		 * @return true they
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public bool opposite (Vector other) {
			return (x == -other.x && y == -other.y);
		}

		/**
		 * Resize the vector to new x/width and y/high.
		 *
		 * @param x the new {@link x} value.
		 * @param y the new {@link y} value.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void resize (double x, double y) {
			this.x = x;
			this.y = y;
		}

		/**
		 * this vector take the maximum values of this and other vectors.
		 *
		 * let's say this vector is ⟨3, 7⟩ and other vecotr is ⟨0, 11⟩, this
		 * vector will be ⟨3, 11⟩ after calling this method with the previous
		 * parameters.
		 *
		 * @param other Other vector.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void max (Vector other) {
			x = double.max (x, other.x);
			y = double.max (y, other.y);
		}

		/**
		 * this vector take the minimum values of this and other vectors.
		 *
		 * let's say this vector is ⟨3, 7⟩ and other vecotr is ⟨0, 11⟩, this
		 * vector will be ⟨0, 7⟩ after calling this method with the previous
		 * parameters.
		 *
		 * @param other Other vector.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public void min (Vector other) {
			x = double.min (x, other.x);
			y = double.min (y, other.y);
		}

		/**
		 * Create a new {@link Xyz.Vector} object.
		 *
		 * @param x Is the x-axis direction of this vector.
		 * @param y Is the y-axis direction of this vector.
		 * @since 0.1
		 * @see Vector.from_vector
		 * @see Vector.from_points
		 */
		[Version (since = "0.1")]
		public Vector (double x = 0.0, double y = 0.0) {
			this.x = x;
			this.y = y;
		}

		/**
		 * Create a new {@link Xyz.Vector} object from other vector
		 *
		 * This method will copy the other vector values.
		 * @param other The vector that will be copied.
		 * @since 0.1
		 * @see Vector
		 * @see Vector.from_points
		 */
		[Version (since = "0.1")]
		public Vector.from_vector (Vector other) {
			this (other.x, other.y);
		}

		/**
		 * Create a new {@link Xyz.Vector} object from two points
		 *
		 * This method will create a vector that have the length between
		 * the two points.<<BR>>
		 * Example:
		 * {{{
		 * using Xyz;
		 *
		 * void main () {
		 *     var a = new Point (4.0, 2.0);
		 *     var b = new Point (1.0, 8.0);
		 *
		 *     var vector = new Vector.from_points (a, b);
		 *     stdout.printf (@"$vector\n");
		 *     // Output: ``⟨-3, 6⟩``
		 * }
		 * }}}
		 * and this is the needed vector for 'a' point to reach 'b' point location.
		 *
		 * @param a First point.
		 * @param b last point.
		 * @since 0.1
		 * @see Vector
		 * @see Vector.from_vector
		 */
		[Version (since = "0.1")]
		public Vector.from_points (Point a, Point b) {
			this (b.x - a.x, b.y - a.y);
		}

		/**
		 * Return a string represent a vector
		 *
		 * @return Vector representation in string.
		 * @since 0.1
		 */
		[Version (since = "0.1")]
		public string to_string () {
			return @"⟨$x, $y⟩";
		}
	}
}
